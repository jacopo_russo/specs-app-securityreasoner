package eu.specsproject.app.securityreasoner.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class Item {

	 public Item(){
	        //Zero args constructor
	    }

	    @XmlAttribute
	    public String id;

	    @XmlValue
	    public String itemValue;

	    public Item(String id, String itemValue){
	        this.id = id;
	        this.itemValue = itemValue; 
	    }
}
