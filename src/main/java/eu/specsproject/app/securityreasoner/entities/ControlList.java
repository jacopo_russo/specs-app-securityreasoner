package eu.specsproject.app.securityreasoner.entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ControlList {
	
	private List<String> controls;
	
	public ControlList(){
		setControls(new ArrayList<String>());
	}

	@XmlElement(name="ccm_control")
	public List<String> getControls() {
		return controls;
	}
	
	public void setControls(List<String> controls) {
		this.controls = controls;
	}
	
	
}
