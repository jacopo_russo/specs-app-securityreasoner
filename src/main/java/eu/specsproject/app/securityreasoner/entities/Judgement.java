package eu.specsproject.app.securityreasoner.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Judgement implements Serializable{
	
	private static final long serialVersionUID = -1425176158087978066L;
	private String id;
    private String judgementXmlDocument;
    private boolean defaultJudgement;

    public Judgement(){
    	defaultJudgement = false;
       //Zero arguments constructor
    }
   
	@XmlAttribute
    public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public Judgement(String doc){
        this.judgementXmlDocument = doc;
    }

    public String getJudgementXmlDocument() {
        return judgementXmlDocument;
    }

    public void setJudgementXmlDocument(
            String caiqXmlDocument) {
        this.judgementXmlDocument = caiqXmlDocument;
    }

    @XmlAttribute(name="default")
	public boolean isDefaultJudgement() {
		return defaultJudgement;
	}

	public void setDefaultJudgement() {
		this.defaultJudgement = true;
	}

}
