package eu.specsproject.app.securityreasoner.entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CoverageReport {
	
	private double coverageScore;
	private List<String> missingControls;
	
	public CoverageReport(){
		missingControls = new ArrayList<String>();
	}
	
	@XmlElement
	public double getCoverageScore() {
		return coverageScore;
	}
	public void setCoverageScore(double coverageScore) {
		this.coverageScore = coverageScore;
	}
	
	@XmlElementWrapper(name="missingControls")
	@XmlElement(name="missingControl")
	
	public List<String> getMissingControls() {
		return missingControls;
	}
	public void setMissingControls(List<String> missingControls) {
		this.missingControls = missingControls;
	}
	
	

}
