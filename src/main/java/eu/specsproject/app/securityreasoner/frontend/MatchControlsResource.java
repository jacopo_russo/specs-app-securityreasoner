package eu.specsproject.app.securityreasoner.frontend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.data_structures.exceptions.ValueNotSetException;
import eu.specs.negotiation.rem.representation.parsers.exceptions.NodeAlreadyPresentException;
import eu.specsproject.app.securityreasoner.entities.ControlList;
import eu.specsproject.app.securityreasoner.entities.CoverageReport;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;
import eu.specsproject.app.securityreasoner.parser.ControlListParser;
import eu.specsproject.app.securityreasoner.parser.CoverageReportParser;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;

public class MatchControlsResource {

	private static final Logger logger = LogManager.getLogger(CaiqTreeResource.class);

	@Context
	UriInfo uriInfo;

	private String caiqId;

	public MatchControlsResource (String id){
		this.caiqId=id;
	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getCoverageReport(String controlList){

		PersistenceImplementation pi = new PersistenceImplementation();
		Tree<InterfaceValueNode, InterfaceEdge> caiq = null;
		CoverageReport report = null;
		ControlList ctrl = new ControlList();
		try {
			
			ctrl = ControlListParser.unmarshal(controlList);
			String caiqDoc = pi.retrieveCaiqDocument(pi.retrieveCaiq(this.caiqId).getDocumentId()).getcaiqXmlDocument();
			caiq = CaiqParser.unmarshall(caiqDoc);

			report = evaluateControlsCoverage(ctrl, caiq);
			return Response.status(200).type("application/xml")
					.entity(CoverageReportParser.marshal(report))
					.type(MediaType.APPLICATION_XML).build();
			
		} catch (JAXBException e) {
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		} catch (NodeAlreadyPresentException | IOException | XMLStreamException | SAXException | ValueNotSetException e) {
			logger.error("getCoverageReport: I/O failure or parsing error" + e);
			return Response.status(500).type("text/plain")
					.entity("Server Error, try again later.").build();
		}

	}



	private static CoverageReport evaluateControlsCoverage(ControlList ctrl,Tree<InterfaceValueNode, InterfaceEdge> tree) throws ValueNotSetException{		

		List<String> ccmSecurityControlList = new ArrayList<String>();

		ccmSecurityControlList = ctrl.getControls();
		int controlsSize = ccmSecurityControlList.size();

		float score = 0;

		//Match request and offer (evaluate score)
		java.util.Collection<InterfaceValueNode> collection = tree.getVertices();
		for (InterfaceValueNode node : collection){
			if(ccmSecurityControlList.contains(node.getName())){
				for (InterfaceValueNode son : tree.getChildren(node) ){
					try {
						if(son.getValue().equals("YES")){
							score ++;
							ccmSecurityControlList.remove(tree.getParent(son).getName());
							//System.out.println("Controls: " + son.getName());
							break;
						}
					} catch (ValueNotSetException e) {
						throw e;
					}
				}
			}
		}

		CoverageReport report = new CoverageReport();
		report.setCoverageScore(score/controlsSize);
		report.setMissingControls(ccmSecurityControlList);

		return report;
	}

//----------------------------------------------------------------------------------------------------------------------------------------------------------
	//Coverage with Sla Template as Input
	/*

public class MatchControlsResource {

	private static final Logger logger = LogManager.getLogger(CaiqTreeResource.class);

	@Context
	UriInfo uriInfo;

	private String caiqId;

	public MatchControlsResource (String id){
		this.caiqId=id;
	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response getCoverageReport(String ComponentSLA){

		PersistenceImplementation pi = new PersistenceImplementation();
		AgreementOffer offer = null;
		Tree<InterfaceValueNode, InterfaceEdge> caiq = null;
		CoverageReport report = null;
		try {
			
			offer = SLAParser.buildOfferFromXml(ComponentSLA);
			String caiqDoc = pi.retrieveCaiqDocument(pi.retrieveCaiq(this.caiqId).getDocumentId()).getcaiqXmlDocument();
			caiq = CaiqParser.unmarshall(caiqDoc);

			report = evaluateControlsCoverage(offer, caiq);
			return Response.status(200).type("application/xml")
					.entity(CoverageReportParser.marshal(report))
					.type(MediaType.APPLICATION_XML).build();
			
		} catch (JAXBException e) {
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		} catch (NodeAlreadyPresentException | IOException | XMLStreamException | SAXException | ValueNotSetException e) {
			logger.error("getCoverageReport: I/O failure or parsing error" + e);
			return Response.status(500).type("text/plain")
					.entity("Server Error, try again later.").build();
		}

	}

	private static List<String> extractControls(AgreementOffer offer){

		List<String> ccmSecurityControlList = new ArrayList<String>();
		//Extract desired controls from SLA 
		List<Term> terms = offer.getTerms().getAll().getAll();
		List<AbstractSecurityControl> securityControl = new ArrayList<AbstractSecurityControl>();
		try{
			for (Term term : terms){
				if(term instanceof ServiceDescriptionTerm){

					ServiceDescriptionTerm serviceTerm = (ServiceDescriptionTerm) term;
					List<CapabilityType> capabilities = serviceTerm.getServiceDescription().getCapabilities().getCapability();

					for (CapabilityType capability : capabilities){

						securityControl = capability.getControlFramework().getSecurityControl();

						Pattern p = Pattern.compile("[A-Z]{3}-[0-9]{2}");
						Matcher matcher = p.matcher(securityControl.toString());
						while (matcher.find()) {									
							ccmSecurityControlList.add(matcher.group());
						} 
					}			
				}
			}
		}catch(NullPointerException e){
			throw e;
		}
		return ccmSecurityControlList; 

	}

	private static CoverageReport evaluateControlsCoverage(AgreementOffer offer,Tree<InterfaceValueNode, InterfaceEdge> tree) throws ValueNotSetException{		

		List<String> ccmSecurityControlList = new ArrayList<String>();

		ccmSecurityControlList = extractControls(offer);
		int controlsSize = ccmSecurityControlList.size();

		float score = 0;

		//Match request and offer (evaluate score)
		java.util.Collection<InterfaceValueNode> collection = tree.getVertices();
		for (InterfaceValueNode node : collection){
			if(ccmSecurityControlList.contains(node.getName())){
				for (InterfaceValueNode son : tree.getChildren(node) ){
					try {
						if(son.getValue().equals("YES")){
							score ++;
							ccmSecurityControlList.remove(tree.getParent(son).getName());
							System.out.println("Controls: " + son.getName());
							break;
						}
					} catch (ValueNotSetException e) {
						throw e;
					}
				}
			}
		}

		CoverageReport report = new CoverageReport();
		report.setCoverageScore(score/controlsSize);
		report.setMissingControls(ccmSecurityControlList);

		return report;
	}

	public static void main(String[] arg) throws Exception

	{
		FromFileToString ffts = new FromFileToString();
		try {
			String sla = ffts.convert("D:\\Users\\Jacopo\\workspace\\specs-app-SecurityReasoner\\specs-SLATemplate-SecureStorage_M30_CCM.xml");
			AgreementOffer offer = SLAParser.buildOfferFromXml(sla);

			String caiqDoc = ffts.convert("D:\\Users\\Jacopo\\workspace\\specs-app-SecurityReasoner\\Dummy-EC2-CAIQ-CCM-3.1.xml");
			Tree<InterfaceValueNode, InterfaceEdge> caiq = CaiqParser.unmarshall(caiqDoc);

			CoverageReport report = evaluateControlsCoverage(offer, caiq);
			System.out.println("Coverage score: " + report.getCoverageScore());
			System.out.println("Missing controls: " + report.getMissingControls());
		} catch (Exception e) {
			throw e;
		}
	}

}
*/
	 

}
