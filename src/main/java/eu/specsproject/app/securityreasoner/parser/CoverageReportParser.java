package eu.specsproject.app.securityreasoner.parser;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import eu.specsproject.app.securityreasoner.entities.CoverageReport;

public class CoverageReportParser {

	public static String marshal(CoverageReport report) throws JAXBException{
		java.io.StringWriter sw = new StringWriter();

		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(CoverageReport.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(report, sw);
		}catch(JAXBException e){
			throw e;
		}
		return sw.toString();

	}


}
