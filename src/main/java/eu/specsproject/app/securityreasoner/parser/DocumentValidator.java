package eu.specsproject.app.securityreasoner.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class DocumentValidator {

	public static void validate(InputStream xml, InputStream xsd) throws SAXException, IOException
	{

		SchemaFactory factory = 
				SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		Schema schema;
		try {
			schema = factory.newSchema(new StreamSource(xsd));

			Validator validator = schema.newValidator();

			Reader reader = new InputStreamReader(xml,"UTF-8");

			validator.validate(new StreamSource(reader));
		} catch (SAXException e) {
			throw e;
		} catch (UnsupportedEncodingException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

	}

}
