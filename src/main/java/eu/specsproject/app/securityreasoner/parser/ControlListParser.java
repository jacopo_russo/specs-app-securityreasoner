package eu.specsproject.app.securityreasoner.parser;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import eu.specsproject.app.securityreasoner.entities.ControlList;

public class ControlListParser {
	
	public ControlListParser(){}
	
	public static String marshal(ControlList controlList) throws JAXBException{
		java.io.StringWriter sw = new StringWriter();

		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(ControlList.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(controlList, sw);
		}catch(JAXBException e){
			throw e;
		}
		return sw.toString();

	}
	

	public static ControlList unmarshal(String xml){
		ControlList list = null;
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(ControlList.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			list = (ControlList) jaxbUnmarshaller.unmarshal(new StringReader(xml));

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return list;
	}


}
