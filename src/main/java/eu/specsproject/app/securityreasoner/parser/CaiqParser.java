package eu.specsproject.app.securityreasoner.parser;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.data_structures.exceptions.ValueNotSetException;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryTreeValue;
import eu.specs.negotiation.rem.representation.data_structures.factories.InterfaceFactoryValueNode;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.DelegateFactoryTreeValue;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryEdge;
import eu.specs.negotiation.rem.representation.data_structures.factories.impl.FactoryValueNode;
import eu.specs.negotiation.rem.representation.parsers.exceptions.NodeAlreadyPresentException;
import eu.specsproject.app.securityreasoner.utility.FromFileToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;
import eu.specsproject.datamodel.generated.TreeValueInstantiator;

public class CaiqParser {

	public static Tree<InterfaceValueNode, InterfaceEdge> unmarshall(String xml) throws NodeAlreadyPresentException, IOException, XMLStreamException , SAXException {
		FromFileToString converter = new FromFileToString();
		String path = (CaiqParser.class.getResource("/")).toString().substring(5).replace("%20", " ");
		String xsd;
		Tree<InterfaceValueNode, InterfaceEdge> tree = null;

		try {
			xsd = converter.convert(path + PropertiesManager.getProperty("xsdValueTree"));
			InterfaceFactoryTreeValue factory_tree = new DelegateFactoryTreeValue();
			InterfaceFactoryValueNode factory_node = new FactoryValueNode();
			InterfaceFactoryEdge factory_edge = new FactoryEdge();
			TreeValueInstantiator tri = new TreeValueInstantiator(factory_tree,
					factory_node, factory_edge);

			try {
				tree = tri.instantiate(xml, xsd);
			} catch (XMLStreamException | SAXException e) {
				throw e;
			}
		} catch (NodeAlreadyPresentException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

		return tree;
	}


	public static String marshall(Tree<InterfaceValueNode, InterfaceEdge> tree) throws XMLStreamException, ValueNotSetException{
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		outputFactory.setProperty("javax.xml.stream.isRepairingNamespaces", Boolean.TRUE);
		String output = null;
		StringWriter writer = new StringWriter();
		try
		{
			XMLStreamWriter streamWriter = outputFactory.createXMLStreamWriter(writer);
			streamWriter.writeStartDocument("utf-8", "1.0");
			streamWriter.writeCharacters("\n");
			streamWriter.writeStartElement("tns", "ValueTree", "ValueTree");
			streamWriter.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			streamWriter.writeAttribute("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation", "ValueTree ValueTree.xsd");
			InterfaceValueNode root = (InterfaceValueNode)tree.getRoot();
			String idRoot = root.getName();
			streamWriter.writeCharacters("\n\n    ");
			streamWriter.writeStartElement("Root");
			streamWriter.writeAttribute("Id", idRoot);
			String spaces = "\n";
			writeFirstLevel(streamWriter, root, tree, spaces);
			streamWriter.writeCharacters("\n");
			streamWriter.writeEndDocument();
			streamWriter.writeCharacters("\n");
			streamWriter.flush();
		}
		catch(XMLStreamException e)
		{
			throw e;
		} catch (ValueNotSetException e) {
			throw e;
		}
		output = writer.toString();
		return output;
	}

	private static void writeFirstLevel(XMLStreamWriter streamWriter, InterfaceValueNode father, Tree<InterfaceValueNode, InterfaceEdge> tree, String spaces) throws XMLStreamException, ValueNotSetException
	{
		Collection<InterfaceValueNode> collection = tree.getChildren(father);
		Iterator<InterfaceValueNode> iterator = collection.iterator();
		Vector<InterfaceValueNode> vett = new Vector<InterfaceValueNode>();
		String sp = spaces.concat("         ");
		for(int i = 0; i < collection.size(); i++)
		{
			InterfaceValueNode node = (InterfaceValueNode)iterator.next();
			vett.add(node);
		}

		for(int i = 0; i < vett.size(); i++)
		{
			InterfaceValueNode node_in = (InterfaceValueNode)vett.get(i);
			String name = node_in.getName();
			Collection<InterfaceEdge> collection_edges = tree.getInEdges(node_in);
			Iterator<InterfaceEdge> iterator_edges = collection_edges.iterator();
			iterator_edges.next();
			try
			{
				streamWriter.writeCharacters(sp);
				streamWriter.writeStartElement("ValueNode");
				streamWriter.writeAttribute("Id", name);
				write_requirement(streamWriter, node_in, tree, spaces);
				streamWriter.writeCharacters(sp);
				streamWriter.writeEndElement();
			}
			catch(XMLStreamException e)
			{
				throw e;
			} catch (ValueNotSetException e) {
				throw e;
			}
		}

	}

	private static void write_requirement(XMLStreamWriter writer, InterfaceValueNode father, Tree<InterfaceValueNode, InterfaceEdge> tree, String spaces) throws XMLStreamException, ValueNotSetException
	{
		Collection<InterfaceValueNode> collection = tree.getChildren(father);
		Iterator<InterfaceValueNode> iterator = collection.iterator();
		Vector<InterfaceValueNode> vett = new Vector<InterfaceValueNode>();
		String sp = spaces.concat("             ");
		for(int i = 0; i < collection.size(); i++)
		{
			InterfaceValueNode node = (InterfaceValueNode)iterator.next();
			vett.add(node);
		}

		try
		{
			for(int i = 0; i < vett.size(); i++)
			{
				InterfaceValueNode node_in = (InterfaceValueNode)vett.get(i);
				String name = node_in.getName();
				Collection<InterfaceEdge> collection_edges = tree.getInEdges(node_in);
				Iterator<InterfaceEdge> iterator_edges = collection_edges.iterator();
				iterator_edges.next();
				writer.writeCharacters(sp);
				writer.writeStartElement("ValueNode");
				writer.writeAttribute("Id", name);
				write_requirement2(writer, node_in, tree, spaces);
				writer.writeCharacters(sp);
				writer.writeEndElement();
			}

		}
		catch(XMLStreamException e)
		{
			throw e;
		} catch (ValueNotSetException e) {
			throw e;
		}
	}

	private static void write_requirement2(XMLStreamWriter writer, InterfaceValueNode father, Tree<InterfaceValueNode, InterfaceEdge> tree, String spaces) throws XMLStreamException, ValueNotSetException
	{
		Collection<InterfaceValueNode> children_collection = tree.getChildren(father);
		Iterator<InterfaceValueNode> children_iterator = children_collection.iterator();
		try
		{
			String sp = spaces.concat("                 ");
			String sp2 = spaces.concat("                         ");
			for(int i = 0; i < children_collection.size(); i++)
			{
				InterfaceValueNode node = (InterfaceValueNode)children_iterator.next();
				String node_name = node.getName();
				Collection<InterfaceEdge> edges_collection = tree.getInEdges(node);
				Iterator<InterfaceEdge> edges_iterator = edges_collection.iterator();
				edges_iterator.next();
				writer.writeCharacters(sp);
				writer.writeStartElement("ValueNode");
				writer.writeAttribute("Id", node_name);

				writer.writeCharacters(sp2);
				writer.writeStartElement("Value");
				writer.writeCharacters(node.getValue());
				writer.writeEndElement();

				writer.writeEndElement();
			}

		}
		catch(XMLStreamException e)
		{
			throw e;
		}
		catch (ValueNotSetException e) {
			throw e;
		}
	}   
}