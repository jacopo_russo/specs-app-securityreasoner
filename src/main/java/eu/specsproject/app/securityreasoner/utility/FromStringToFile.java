package eu.specsproject.app.securityreasoner.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class FromStringToFile
implements InterfaceFromStringToFile
{
	public void convert(String xml, String path)
			throws UnsupportedEncodingException, FileNotFoundException, IOException
	{
		try
		{
			FileOutputStream file = new FileOutputStream(path);
			OutputStreamWriter writer = new OutputStreamWriter(file, "UTF-8");
			writer.write(xml);
			writer.close();
		}
		catch (UnsupportedEncodingException e)
		{
			throw e;
		}

		catch (FileNotFoundException e)
		{
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
}