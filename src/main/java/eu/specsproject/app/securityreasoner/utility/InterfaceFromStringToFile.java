package eu.specsproject.app.securityreasoner.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public abstract interface InterfaceFromStringToFile
{
  public abstract void convert(String paramString1, String paramString2)
		  throws UnsupportedEncodingException, FileNotFoundException, IOException;
}
