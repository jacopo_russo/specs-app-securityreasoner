package eu.specsproject.app.securityreasoner.utility;

import java.io.IOException;

public abstract interface InterfaceFromFileToString
{
  public abstract String convert(String paramString)
    throws IOException;
}
